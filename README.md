# Git Tools

This repository is a collection of git scripts that the developers can
use to help speed up their git workflow.

For each script you wish to use:

 do this, for example:

`sudo cp git-check /usr/local/bin/git-check`

`sudo chmod 775 /usr/local/bin/git-check`

### git-check

This script displays the branch of each git repo in the current directory

`git-check`

### git-command-all

This script sends a git command to each git repo in the current directory

`git-command-all <git command>`
